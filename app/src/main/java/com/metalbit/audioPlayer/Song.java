package com.metalbit.audioPlayer;

public class Song {
    private String name;
    private String path;
    private final int index;
    private String format;

    public Song(String newPath, String newName, int newIndex, String newFormat)
    {
        name = newName;
        path = newPath;
        index = newIndex;
        format = newFormat;
    }

    public String getName()
    {
        return name;
    }

    public String getPath()
    {
        return path;
    }

    public int getIndex()
    {
        return index;
    }

    public String getFormat()
    {
        return format;
    }

    public void copy(Song second)
    {
        String auxPath = second.getPath();
        String auxName = second.getName();
        String auxFormat = second.getFormat();

        path = auxPath;
        name = auxName;
        format = auxFormat;
    }
}
