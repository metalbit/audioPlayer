package com.metalbit.audioPlayer;

import java.util.List;

interface CallbackInterface
{
    public void getData(List<com.metalbit.audioPlayer.Song> songs);
}
