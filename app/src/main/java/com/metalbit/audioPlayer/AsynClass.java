package com.metalbit.audioPlayer;

import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsynClass extends AsyncTask<Void, Void, Void> {

    List<com.metalbit.audioPlayer.Song> songs = new ArrayList<com.metalbit.audioPlayer.Song>();
    com.metalbit.audioPlayer.CallbackInterface privateCallback;
    private int currentIndex = 0;

    public AsynClass(com.metalbit.audioPlayer.CallbackInterface callback)
    {
        privateCallback = callback;
    }

    protected Void doInBackground(Void... params)
    {
        scan("/sdcard");
        return null;
    }

    protected void onPostExecute(Void result)
    {
        privateCallback.getData(songs);
    }


    private void scan(String path)
    {
        try
        {
            if (path.equals(".") || path.equals("..")) {
                return;
            }

            File root = new File(path);

            File[] filesInDir = root.listFiles();

            if (filesInDir == null)
            {
                return;
            }

            for (File file : filesInDir)
            {
                if (file.isDirectory())
                {
                    scan(file.getPath());
                }
                else
                {
                    for (String format : AudioFormats.AudioFormats) {
                        if (file.getName().endsWith(format))
                        {
                            Song newSong = new Song(file.getPath(), file.getName(), currentIndex, format);
                            if (!containsSong(songs, file.getName())) {
                                songs.add(newSong);
                                currentIndex++;
                            }
                        }
                    }

                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean containsSong(List<Song> songs, String songName) {
        for (int index = 0; index < songs.size(); index++) {
            if (songs.get(index).getName().equals(songName)) {
                return true;
            }
        }
        return false;
    }
}
