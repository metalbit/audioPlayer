package com.metalbit.audioPlayer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements CallbackInterface, AdapterView.OnItemSelectedListener  {

    private LinearLayout layout;
    private MediaPlayer mediaPlayer;

    private int currentSongIndex = -1;
    private boolean replaySong = false;
    private boolean randomSong = false;

    private List<Song> songs = new ArrayList<Song>();
    private List<Song> selectedSongs;

    private NavigationView navigationView;
    private MenuItem songStateItem = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int[] initStarted = {0};

        setContentView(R.layout.activity_main);

        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && initStarted[0] == 0) {
                    initStarted[0]++;
                    init();
                    cancel();
                }
            }

            public void onFinish() {
                if (initStarted[0] == 0) {
                    System.exit(0);
                }
            }
        }.start();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            try {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        else
        {
            if (initStarted[0] == 0) {
                initStarted[0]++;
                init();
            }
        }
    }

    private void init() {
        CallbackInterface myInterface = MainActivity.this;
        new AsynClass(myInterface).execute(null, null, null);

        navigationView = findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                item.setCheckable(true);

                switch (item.getItemId())
                {
                    case R.id.songStatus:
                        songStateItem = songStateItem == null ? item : songStateItem;

                        if (mediaPlayer.isPlaying())
                        {
                            mediaPlayer.pause();
                            item.setTitle("Resume");
                        }
                        else
                        {
                            mediaPlayer.start();
                            item.setTitle("Pause");
                        }
                        break;

                    case R.id.repeatSong:
                        replaySong = !replaySong;
                        StringBuilder newTitle = new StringBuilder();
                        newTitle.append("Replay current song");

                        if (replaySong)
                        {
                            newTitle.append(" (On)");
                        }
                        else
                        {
                            newTitle.append(" (Off)");
                        }
                        item.setTitle(newTitle.toString());

                        break;

                    case R.id.randomSong:
                        randomSong = !randomSong;

                        if (randomSong) {
                            item.setTitle("Random (On)");
                        } else {
                            item.setTitle("Random (Off)");
                        }

                        break;
                }

                return true;
            }
        });

        LinearLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(Color.BLACK);

        layout = findViewById(R.id.layout);
        layout.setBackgroundColor(Color.BLACK);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioAttributes(
                new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
        );

        mediaPlayer.setWakeMode(getApplicationContext(),1);

        mediaPlayer.setOnCompletionListener( new MediaPlayer.OnCompletionListener(){
            public void onCompletion(MediaPlayer mp)
            {
                try
                {
                    if (!replaySong) {
                        currentSongIndex++;

                        if (currentSongIndex > selectedSongs.size() - 1) {
                            currentSongIndex = 0;
                        }
                    }

                    if (randomSong) {
                        Random random = new Random();
                        currentSongIndex = random.nextInt(selectedSongs.size());
                    }

                    mediaPlayer.stop();
                    mediaPlayer.reset();

                    Uri songPath = Uri.parse(selectedSongs.get(currentSongIndex).getPath());
                    mediaPlayer.setDataSource(getApplicationContext(), songPath);

                    mediaPlayer.prepare();
                    mediaPlayer.start();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.audioFormatSpinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

        for (String format : AudioFormats.AudioFormats) {
            adapter.add(format);
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    public void getData(final List<Song> foundSongs)
    {
        songs = foundSongs;
        List<Song> filteredSongs = filter(songs, AudioFormats.AllFormats);
        List<Song> sortedSongs = sort(filteredSongs);

        selectedSongs = sortedSongs;

        displaySongs(sortedSongs);
    }

    private void displaySongs(List<Song> songsToDisplay) {
        layout.removeAllViews();

        for (int i = 0; i < songsToDisplay.size(); i++)
        {
            Button newSong = new Button(this);
            newSong.setBackgroundColor(Color.BLACK);
            newSong.setTextColor(Color.WHITE);
            newSong.setText(songsToDisplay.get(i).getName());

            final int finalI = i;
            newSong.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri songPath = Uri.parse(songsToDisplay.get(finalI).getPath());

                    try
                    {
                        mediaPlayer.stop();
                        mediaPlayer.reset();

                        mediaPlayer.setDataSource(getApplicationContext(), songPath);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        currentSongIndex = songsToDisplay.get(finalI).getIndex();

                        if (songStateItem != null)
                        {
                            songStateItem.setTitle("Pause");
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            layout.addView(newSong);
        }
    }

    private List<Song> sort(List<Song> songs)
    {
        List<Song> sortedSongs = songs;
        Song aux = new Song("","",0, "");

        for (int i = 0; i < sortedSongs.size(); i++)
        {
            for (int j = i + 1; j < sortedSongs.size(); j++)
            {
                String name1, name2;
                name1 = sortedSongs.get(i).getName();
                name2 = sortedSongs.get(j).getName();

                if (compare(name1, name2) == -1)
                {
                    aux.copy(sortedSongs.get(i));
                    sortedSongs.get(i).copy( sortedSongs.get(j) );
                    sortedSongs.get(j).copy( aux );
                }
            }
        }

        return sortedSongs;
    }

    private List<Song> filter(List<Song> songs, String format) {
        List<Song> filteredSongs = new ArrayList<Song>();

        for (int i = 0; i < songs.size(); i++) {
            if (songs.get(i).getFormat().equals(format) || AudioFormats.SelectedFormat.equals(AudioFormats.AllFormats)) {
                filteredSongs.add(songs.get(i));
            }
        }

        return filteredSongs;
    }

    private int compare(String a, String b)
    {
        int size;
        if (a.length() < b.length())
        {
            size = a.length();
        }
        else
        {
            size = b.length();
        }

        for (int i = 0; i < size; i++)
        {
            if ((int)a.charAt(i) > 64 && (int)a.charAt(i) < 91)
            {
                a = a.replace(a.charAt(i), (char) ((int)a.charAt(i) + 32));
            }
            if ((int)b.charAt(i) > 64 && (int)b.charAt(i) < 91)
            {
                b = b.replace(b.charAt(i), (char) ((int)b.charAt(i) + 32));
            }

            if (a.charAt(i) < b.charAt(i))
            {
                return 1;
            }
            else if (a.charAt(i) > b.charAt(i))
            {
                return -1;
            }
        }
        return 0;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        String format = parent.getItemAtPosition(pos).toString();

        AudioFormats.SelectedFormat = format;

        List<Song> filteredSongs = filter(songs, format);
        List<Song> sortedSongs = sort(filteredSongs);

        selectedSongs = sortedSongs;

        displaySongs(sortedSongs);
    }

    public void onNothingSelected(AdapterView<?> parent) {}
}

