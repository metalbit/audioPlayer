package com.metalbit.audioPlayer;

import java.util.Arrays;
import java.util.List;

public class AudioFormats {

    public static List<String> AudioFormats = Arrays.asList(
            "all",
            ".mp3",
            ".flac",
            ".3gp",
            ".mp4",
            ".m4a",
            ".aac",
            ".amr",
            ".mid",
            ".xmf",
            ".mxmf",
            ".rtttl",
            ".rtx",
            ".ota",
            ".imy",
            ".mkv",
            ".ogg",
            ".wav"
        );

    public static String AllFormats = "all";
    public static String SelectedFormat = AllFormats;
}
